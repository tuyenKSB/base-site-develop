var gulp = require('gulp');
var sass = require('gulp-sass');
var wait = require('gulp-wait');
var del = require('del'); 
var sourcemaps = require('gulp-sourcemaps');
var browserSync = require('browser-sync').create();
var fileInclude = require('gulp-file-include');
let webpack = require('webpack-stream');
var autoprefixer = require("gulp-autoprefixer");



gulp.task('browserSync', function (done) {
    browserSync.init({
        server: {
            proxy: "localhost:3001",
            baseDir: './dist',
            open: true,
            online: false,
            ghostMode: {
                scroll: true
            },
            logLevel: 'info', // 'debug', 'info', 'silent', 'warn'
            logConnections: false,
            logPrefix: "Browser-Sync",
            notify: true
        },
    });
    done();
});


gulp.task('sass', function () {
    return gulp.src('_src/sass/styles.scss')
        .pipe(sourcemaps.init())
        .pipe(wait(200))
        .pipe(sass().on('error', sass.logError)) // Converts Sass to CSS with gulp-sass
        .pipe(autoprefixer({
            cascade: false
        }))
        .pipe(sourcemaps.write('./map'))

        .pipe(gulp.dest('./dist/css')) // Outputs
        .pipe(browserSync.reload({
            stream: true
        }))
});

gulp.task('webpack', (done) => {
     return gulp.src('_src/')
        .pipe(webpack(require('./webpack.config.js')))
        .pipe(gulp.dest('./dist/js'))
        .pipe(browserSync.reload({
            stream: true
        }))
});



gulp.task('file-include', function (done) {
    gulp.src(['_modules/pages/**/**/*.html'])
        .pipe(fileInclude({
            prefix: '@@',
            basepath: '@file'
        }))
        .pipe(gulp.dest('./dist'))

    done()
});

gulp.task('clean:dist', function(done) {
    del.sync(['dist/**/*', '!dist/images', '!dist/images/**/*']);
    done();
})


gulp.task('copy', function (done) {
    gulp.src(['_modules/pages/assets/**/*']).pipe(gulp.dest('./dist/assets'));
    gulp.src(['_src/app/**/*']).pipe(gulp.dest('./dist/app'));
    done();
})



gulp.task('watch', function (done) {
    gulp.watch('_src/sass/**/*.scss', gulp.task('sass'));
    gulp.watch('_modules/pages/assets/**/*', gulp.task('copy'));
    gulp.watch('_src/js/**/*.js', gulp.series('webpack'))
    gulp.watch('_modules/**/**/*.html', gulp.task('file-include'));
    done();
})
gulp.task('reload', function () {
    gulp.watch('dist/**/**/*.*', browserSync.reload);
})

gulp.task('default', (done) => {
    gulp.series('watch', () => {
        gulp.watch('dist/**/**', browserSync.reload())
    })
    done()
})

gulp.task('default', gulp.series('clean:dist', 'sass', 'webpack', 'file-include', 'copy', 'browserSync', 'watch', 'reload', function (done) {
    done();
}));

gulp.task('build', gulp.series('clean:dist', 'sass', 'webpack', 'file-include', 'copy', function (done) {
    done();
}));
// skip if error occured
function swallowError(error) {
    console.log(error.toString())
    this.emit('end')
}